#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern FILE *yyin, *yyout;
extern int yyparse(void);
extern int total_errors;

const char *input_file = NULL;

int main(int argc, char **argv)
{
	++argv, --argc;  /* skip over program name */

	if (argc > 0) {
		input_file = argv[0];
		FILE *fp = fopen(argv[0], "r");
		if (fp == NULL) {
			fprintf(stderr, "File doesn't exist!\n");
			return 2;
		}
		yyin = fopen(argv[0], "r");
	}
	else
		yyin = stdin;

	printf("Parsing file: %s\n\n", input_file);
	yyparse();

	if (total_errors == 0)
		printf("\n\nProgram is correct.\n");
	else {
		printf("\n\nProgram contains errors.\n");
		printf("Total errors in program = %d\n", total_errors);
	}

	return 0;
}
