%{
#include <stdio.h>
#include <stdlib.h>

extern int line;
extern int column;
extern int yyleng;
const char *input_file;
int message_length;
int total_errors = 0;
int post_flag = 0;
int cl_flag = 0;
%}

/* Output informative error messages (bison Option) */
%error-verbose

%token SP CRLF
%token GET HEAD POST
%token HTTP_VERSION
%token ABSOLUTE_URI RELATIVE_URI AUTHORITY
%token CONNECTION
%token DATE HTTP_DATE HTTP_TIME
%token TRANSFER_ENCODING CHUNKED GZIP DEFLATE
%token ACCEPT_CHARSET Q_VALUE
%token REFERER
%token USER_AGENT PRODUCT COMMENT
%token CONTENT_LENGTH EXPIRES
%token NUMBER TOKEN MESSAGE
%token '*' ':' ',' ';' 'q' '='
%token END 0 "end of file"

%expect 8

%start request_message

%%

request_message : request_line request_headers CRLF message_body END
;
request_headers : %empty
                | header CRLF
                | request_headers header CRLF
                | error SP { yyerrok; }
                | error CRLF { yyerrok; }
;
header : general_header
       | request_header
       | entity_header
;
request_line : method SP request_uri SP HTTP_VERSION CRLF
             | error SP { yyerrok; }
             | error CRLF { yyerrok; }
;
method : GET
       | HEAD
       | POST {post_flag = 1;}
;
request_uri : '*'
            | ABSOLUTE_URI
            | RELATIVE_URI
            | AUTHORITY
;
general_header : connection
               | date
               | transfer_encoding
;
connection : CONNECTION ':' connection_tokens
;
connection_tokens : SP TOKEN
                  | connection_tokens ',' SP TOKEN
;
date : DATE ':' SP HTTP_DATE
;
transfer_encoding : TRANSFER_ENCODING ':' encodings
;
encodings : SP encoding
          | encodings ',' SP encoding
;
encoding : CHUNKED
         | GZIP
         | DEFLATE
;
request_header : accept_charset
               | referer
               | user_agent
;
accept_charset : ACCEPT_CHARSET ':' charsets
;
charsets : SP charset
         | charsets ',' SP charset
;
charset : TOKEN
        | '*'
        | TOKEN ';' 'q' '=' Q_VALUE
        | '*' ';' 'q' '=' Q_VALUE
;
referer : REFERER ':' SP ABSOLUTE_URI
        | REFERER ':' SP RELATIVE_URI
;
user_agent : USER_AGENT ':' agents
;
agents : SP agent
       | agents SP agent
;
agent : PRODUCT
      | COMMENT
;
entity_header : content_length { cl_flag = 1; }
              | expires
;
content_length : CONTENT_LENGTH ':' SP NUMBER { message_length = $4; }
;
expires : EXPIRES ':' SP HTTP_DATE
;
message_body : %empty {
	if(post_flag) {
		yyleng = 0;
		if(cl_flag == 0) {
			yyerror("semantic error, Content-Length header is required when method = POST");
		}
		yyerror("semantic error, message-body is required when method = POST");
	}
}
             | MESSAGE {
	if(post_flag) {
		if(cl_flag == 0) {
			yyleng = 0;
			yyerror("semantic error, Content-Length header is required when method = POST,\neither it does not exist or contains error(s)");
		}
		else if(cl_flag == 1 && message_length != yyleng) {
			printf("message length = %d\n", yyleng);
			yyleng = 0;
			yyerror("semantic error, Content-Length header's value does not match message length!", yyleng);
		}
	}
}
;

%%


int yyerror(const char * msg)
{
	total_errors++;
	fprintf(stderr, "=> PARSE ERROR on line %d, column %d: %s.\n", line, column-yyleng, msg);

	return 1;
}
