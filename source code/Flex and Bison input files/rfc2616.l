%{
#include "rfc2616.tab.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern const char *input_file;
extern int total_errors;
int line = 1;
int column = 1;
int message_body_start = 0;
char seq_new_lines = 0;
%}

%option noyywrap
%option stack
%x mesg

digit				[0-9]
number				{digit}+
http_version		"HTTP/"{number}\.{number}
http_date			{digit}{2}\/{digit}{2}\/{digit}{4}\ {digit}{2}\:{digit}{2}
qvalue 				(0(\.{digit}{0,3})?)|(1(\.0{0,3})?)
/* simple regex for absoluteURI. */
abs_uri				"http:/"(\/[a-zA-Z0-9_:.]+)+(\/)?
/* simple regex for relativeURI. */
rel_uri				(\/[a-zA-Z0-9_.]+)+(\/)?
token				([a-zA-Z0-9\!\#\$\%\&\'\*\+\-\.\^\_\`\|\~])+
product				{token}(\/{token})?
/* simple regex for comment */
comment				\([^()\n]+\)
message				(.|\n)+

%%

"GET" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return GET;
}
"HEAD" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return HEAD;
}
"POST" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return POST;
}
"Connection" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return CONNECTION;
}
"Date" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return DATE;
}
"Transfer-Encoding"	{
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return TRANSFER_ENCODING;
}
"Accept-Charset" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return ACCEPT_CHARSET;
}
"Referer" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return REFERER;
}
"User-Agent" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return USER_AGENT;
}
"Content-Length" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return CONTENT_LENGTH;
}
"Expires" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return EXPIRES;
}
"chunked" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return CHUNKED;
}
"gzip" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return GZIP;
}
"deflate" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return DEFLATE;
}
"authority" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return AUTHORITY;
}
" " {
	seq_new_lines = 0;
	printf("Found space in line %d, column %d\n", line, column);
	column += yyleng;
	return SP;
}
"*" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return '*';
}
":" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return ':';
}
"," {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return ',';
}
";" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return ';';
}
"q" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return 'q';
}
"=" {
	seq_new_lines = 0;
	printf("Found %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return '=';
}
{abs_uri} {
	seq_new_lines = 0;
	printf("Found ABSOLUTE_URI: %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return ABSOLUTE_URI;
}
{rel_uri} {
	seq_new_lines = 0;
	printf("Found RELATIVE_URI: %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return RELATIVE_URI;
}
{http_version} {
	seq_new_lines = 0;
	printf("Found HTTP_VERSION: %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return HTTP_VERSION;
}
{http_date} {
	seq_new_lines = 0;
	printf("Found HTTP_DATE: %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return HTTP_DATE;
}
{qvalue} {
	seq_new_lines = 0;
	printf("Found Q_VALUE: %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return Q_VALUE;
}
{number} {
	seq_new_lines = 0;
	yylval = atoi(yytext);
	printf("Found NUMBER: %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return NUMBER;
}
{token} {
	seq_new_lines = 0;
	printf("Found TOKEN: %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return TOKEN;
}
{product} {
	seq_new_lines = 0;
	printf("Found PRODUCT: %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return PRODUCT;
}
{comment} {
	seq_new_lines = 0;
	printf("Found COMMENT: %s in line %d, column %d\n", yytext, line, column);
	column += yyleng;
	return COMMENT;
}
\n {
	printf("Found CRLF (new line character) in line %d, column %d\n", line, column);
	line++;
	column = 1;
	seq_new_lines++;

	if(seq_new_lines == 2) {
		seq_new_lines = 0;
		BEGIN(mesg);
	}
	return CRLF;
}
<mesg>{message} {
	printf("Found MESSAGE: \"%s\" in line %d, column %d\n", yytext, line, column);
	BEGIN(INITIAL);
	return MESSAGE;
}
. {
	total_errors++;
	fprintf(stderr, "=> PARSE ERROR on line %d, column %d: lexical error, invalid token [ %s ]\n", line, column, yytext);
}

%%
